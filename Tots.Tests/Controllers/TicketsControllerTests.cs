﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tots.Controllers;
using System.Web.Mvc;

namespace Tots.Tests.Controllers
{
    [TestFixture]
    class TicketsControllerTests
    {
        private TicketsController _c = null;

        [SetUp]
        public void SetUp()
        {
            _c = new TicketsController();
        }

        [Test]
        public void Index_withNoArgs_ReturnsValid()
        {
            ViewResult result = _c.Index(null, null) as ViewResult;

            Assert.IsNotNull(result);
        }
    }
}
