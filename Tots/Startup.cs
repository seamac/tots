﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Tots.Startup))]
namespace Tots
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
