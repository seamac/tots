﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tots.Models;

namespace Tots.Data
{
    public interface ITotsUserRepository : IDisposable
    {
        IEnumerable<TotsUser> GetAll();
        TotsUser FindById(string userId);
        void Add(TotsUser user);
        void Update(TotsUser user);
        void Delete(TotsUser user);
        void Save();
    }
}
