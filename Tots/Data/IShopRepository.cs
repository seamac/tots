﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tots.Models;

namespace Tots.Data
{
    public interface IShopRepository : IDisposable
    {
        IEnumerable<Shop> GetAll();
        Shop   FindById(int shopId);
        void   Add     (Shop shop);
        void   Update  (Shop shop);
        void   Delete  (Shop shop);
        void   Save    ();
    }
}
