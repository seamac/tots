﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tots.Models;

namespace Tots.Data
{
    public class ShopRepository : IShopRepository
    {
        private ITotsDbContext _context;

        public ShopRepository(ITotsDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<Shop> GetAll()
        {
            return _context.Shops;
        }

        public Shop FindById(int shopId)
        {
            var shop = _context.Shops
                               .First(s => s.ShopId == shopId);
            return shop;
        }

        public void Add(Shop shop)
        {
            _context.Shops.Add(shop);
        }

        public void Update(Shop shop)
        {
            //_context.Entry(shop).State = System.Data.Entity.EntityState.Modified;
            _context.MakeStateModified(shop);
        }

        public void Delete(Shop shop)
        {
            _context.Shops.Remove(shop);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        // disposable implementation
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Dispose(true);
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}