﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tots.Models;

namespace Tots.Data
{
    public interface ITicketRepository : IDisposable
    {
        IEnumerable<Ticket> GetAll();
        IEnumerable<Ticket> GetActive();
        IEnumerable<Ticket> FindByShopId(int shopId);
        Ticket FindById(int ticketId);
        void   Add     (Ticket ticket);
        void   Update  (Ticket ticket);
        void   Delete  (Ticket ticket);
        void   Save    ();
    }
}
