﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Tots.Models;

namespace Tots.Data
{
    public class TicketRepository : ITicketRepository
    {
        private TotsDbContext _context;

        public TicketRepository(TotsDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<Ticket> GetAll()
        {
                var tickets = _context.Tickets
                                      .Include(t => t.User)
                                      .Include(t => t.Shop);

                return tickets;
        }

        public IEnumerable<Ticket> GetActive()
        {
            var tickets = _context.Tickets
                                  .Where(t => t.Status < 3)
                                  .Include(t => t.User)
                                  .Include(t => t.Shop);
            return tickets;
        }

        public Ticket FindById(int ticketId)
        {
            return _context.Tickets
                           .Where(t => t.TicketId == ticketId)
                           .Include(t => t.User)
                           .Include(t => t.Shop)
                           .FirstOrDefault();
        }

        public IEnumerable<Ticket> FindByShopId(int shopId)
        {
            return _context.Tickets
                           .Where(t => t.ShopId == shopId);
        }

        public void Add(Ticket ticket)
        {
            _context.Tickets.Add(ticket);
        }

        public void Update(Ticket ticket)
        {
            _context.Entry(ticket).State = System.Data.Entity.EntityState.Modified;
        }

        public void Delete(Ticket ticket)
        {
            _context.Tickets.Remove(ticket);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        // disposable implementation
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Dispose(true);
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}