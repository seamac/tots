﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using Tots.Models;
namespace Tots.Data
{
    public interface ITotsDbContext
    {
        DbSet<Employee> Employees { get; set; }
        DbSet<Shop>     Shops { get; set; }
        DbSet<Ticket>   Tickets { get; set; }
        IDbSet<TotsUser> Users { get; set; }

        void MakeStateModified(object item);
        int SaveChanges();
    }
}
