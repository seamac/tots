﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tots.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;


namespace Tots.Data
{
    public class TotsDbContext : IdentityDbContext<TotsUser>, ITotsDbContext
    {

        public TotsDbContext()
            : base("DefaultConnection")
        {

        }


        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Shop> Shops { get; set; }

        public void MakeStateModified(object item)
        {
            Entry(item).State = EntityState.Modified;
        }

    }
}