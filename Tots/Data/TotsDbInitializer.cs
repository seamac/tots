﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Data.Entity.Migrations;
using Tots.Models;

namespace Tots.Data
{
    public class TotsDbInitializer
        : DropCreateDatabaseIfModelChanges<TotsDbContext>
    {
        protected override void Seed(TotsDbContext context)
        {

            // seed admin user
            var passwordHash = new PasswordHasher();
            string password = passwordHash.HashPassword("Password1");
            var adminUser = new TotsUser
            {
                UserName = "Admin",
                PasswordHash = password
            };
            context.Users.AddOrUpdate(adminUser);
            context.SaveChanges();


            Random random = new Random();
            var dateTime = DateTime.Now;

            // seed a few shops
            Shop currentShop;
            for (int x = 0; x < 5; x++)
            {
                currentShop = new Shop
                {
                    Name = String.Format("Shop #{0}", x)
                };
                context.Shops.Add(currentShop);
                context.SaveChanges();

                // seed some work orders and assign to the shop
                for(int y = 0; y < 10; y++)
                {

                    context.Tickets.Add(
                        new Ticket
                        {
                            Description = String.Format("Fix problem number {0}", y),
                            ShopId = currentShop.ShopId,
                            UserId = adminUser.Id,
                            Status = random.Next(0, 4),
                            DateCreated = dateTime,
                            DateModified = dateTime
                        });
                }
            }
             


                base.Seed(context);
        }
    }
}