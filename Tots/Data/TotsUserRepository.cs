﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tots.Models;

namespace Tots.Data
{
    public class TotsUserRepository : ITotsUserRepository
    {
        private ITotsDbContext _context;

        public TotsUserRepository(ITotsDbContext context)
        {
            this._context = context;
        }

        public IEnumerable<TotsUser> GetAll()
        {
            return _context.Users;
        }

        public TotsUser FindById(string userId)
        {
            return _context.Users.FirstOrDefault(u => u.Id == userId);
        }

        public void Add(TotsUser user)
        {
            _context.Users.Add(user);
        }

        public void Update(TotsUser user)
        {
            _context.MakeStateModified(user);
        }

        public void Delete(TotsUser user)
        {
            _context.Users.Remove(user);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        // disposable implementation
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    Dispose(true);
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}