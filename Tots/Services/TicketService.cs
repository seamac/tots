﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tots.Data;
using Tots.Models;
using Tots.Models.ViewModels;

namespace Tots.Services
{
    public class TicketService
    {
        public TicketIndexViewModel getSortedTickets(ITicketRepository ticketRepository, IShopRepository shopRepository, string sortOrder, int? ShopFilter, bool showAll = false)
        {
            // initialize an index viewmodel
            TicketIndexViewModel ticketIndexVM = new TicketIndexViewModel();

            // set the listing parameters
            ticketIndexVM.SortOrder = sortOrder;                                                       // describes how to sort the list (id_asc, shop_asc, etc.)
            ticketIndexVM.IdSortParam = String.IsNullOrEmpty(sortOrder) ? "id_asc" : "";               // sets id sort link action to the opposite (blank instead of id_desc because it's default)
            ticketIndexVM.ShopNameSortParam = sortOrder == "shop_desc" ? "shop_asc" : "shop_desc";     // sets shop sort link action to the opposite
            ticketIndexVM.StatusSortParam = sortOrder == "status_desc" ? "status_asc" : "status_desc"; // sets status sort link action to the opposite
            ticketIndexVM.ShowAllParam = showAll ? "true" : "";                                        // sets the showAll button to the opposite
            ticketIndexVM.ShopFilterParam = ShopFilter;                                                // persist the shop id that is being filtered, or null

            ticketIndexVM.ShopListing = Mapper.Map(shopRepository.GetAll(), new List<ShopViewModel>()); // creates a List<ShopViewModel> to populate a SelectList

            // tickets enumerable declaration
            IEnumerable<Ticket> tickets;

            if (showAll)
            {
                // get all tickets
                tickets = from t in ticketRepository.GetAll()
                          select t;
            }
            else
            {
                // hide completed and cancelled tickets
                tickets = from t in ticketRepository.GetActive()
                          select t;
            }

            // filter by shop id
            if (ShopFilter != null)
            {
                var shopById = shopRepository.FindById((int)ShopFilter);

                if (shopById != null)
                {
                    tickets = ticketRepository.FindByShopId((int)ShopFilter);
                }
                else
                {
                    // that shop doesn't exist.
                    ticketIndexVM.ShopFilterParam = null;
                    ticketIndexVM.Errors.Add(
                        String.Format("No shop exists with the ID# {0}", ShopFilter));
                }
            }

            // handle sorting
            switch (sortOrder)
            {
                case "id_asc":
                    tickets = tickets.OrderBy(t => t.TicketId);
                    break;
                case "shop_desc":
                    tickets = tickets.OrderByDescending(t => t.Shop.Name);
                    break;
                case "shop_asc":
                    tickets = tickets.OrderBy(t => t.Shop.Name);
                    break;
                case "status_desc":
                    tickets = tickets.OrderByDescending(t => t.Status);
                    break;
                case "status_asc":
                    tickets = tickets.OrderBy(t => t.Status);
                    break;
                default:
                    tickets = tickets.OrderByDescending(t => t.TicketId);
                    break;
            }

            var ticketsVM = Mapper.Map(tickets, new List<TicketViewModel>());

            ticketIndexVM.Tickets = ticketsVM;

            return ticketIndexVM;
        }
    }
}