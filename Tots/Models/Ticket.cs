﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tots.Models
{
    public class Ticket
    {
        public int TicketId { get; set; }
        public int ShopId { get; set; }
        public string UserId { get; set; }

        public string Description { get; set; }

        public int Status { get; set; }

        public Shop Shop { get; set; }
        public TotsUser User { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }
}