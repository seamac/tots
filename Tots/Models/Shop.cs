﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tots.Models
{
    public class Shop
    {
        public int ShopId { get; set; }
        public string Name { get; set; }
    }
}
