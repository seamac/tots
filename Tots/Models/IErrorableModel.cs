﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tots.Models
{
    public interface IErrorableModel
    {
        List<string> Errors { get; set; }
    }
}
