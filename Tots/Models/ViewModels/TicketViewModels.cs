﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tots.Models.ViewModels
{
    public class TicketViewModel
    {
        [Display(Name="ID#")]
        public int TicketId { get; set; }
        public int ShopId { get; set; }
        public string UserId { get; set; }
        public string Description { get; set; }

        public int Status { get; set; }

        public ShopViewModel Shop { get; set; }
        public TotsUser User { get; set; }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
    }

    public class TicketIndexViewModel : IErrorableModel
    {
        public TicketIndexViewModel()
        {
            ShopListing = new List<ShopViewModel>();
            Tickets = new List<TicketViewModel>();
            Errors = new List<string>();
        }

        public string SortOrder { get; set; }
        public string IdSortParam { get; set; }
        public string ShopNameSortParam { get; set; }
        public string StatusSortParam { get; set; }
        public string ShowAllParam { get; set; }
        public int? ShopFilterParam { get; set; }

        public List<string> Errors { get; set; }

        public ICollection<ShopViewModel> ShopListing { get; set; }

        public ICollection<TicketViewModel> Tickets;
    }
}