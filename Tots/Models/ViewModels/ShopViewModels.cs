﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tots.Models.ViewModels
{
    public class ShopViewModel
    {
        public int ShopId { get; set; }
        [Display(Name="Shop")]
        public string Name { get; set; }
    }
}