﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tots.Models;
using Tots.Models.ViewModels;

namespace Tots.App_Start
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            AutoMapper.Mapper.CreateMap<Ticket, TicketViewModel>();
            AutoMapper.Mapper.CreateMap<Shop, ShopViewModel>();
            AutoMapper.Mapper.CreateMap<Employee, EmployeeViewModel>();
            AutoMapper.Mapper.CreateMap<EmployeeViewModel, Employee>();
            AutoMapper.Mapper.CreateMap<ShopViewModel, Shop>();
            AutoMapper.Mapper.CreateMap<TicketViewModel, Ticket>();
        }
    }
}