﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tots.Models;
using Tots.Data;
using Tots.Models.ViewModels;
using AutoMapper;
using System.Diagnostics;
using Tots.Services;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Tots.Controllers
{
    [Authorize]
    public class TicketsController : Controller
    {
        private ITicketRepository   ticketRepository;
        private IShopRepository     shopRepository;
        private ITotsUserRepository userRepository;

        private TotsDbContext db = new TotsDbContext();

        public TicketsController()
        {
            this.ticketRepository = new TicketRepository(this.db);
            this.shopRepository   = new ShopRepository(db);
            this.userRepository   = new TotsUserRepository(db);
        }

        public TicketsController(ITicketRepository ticketRepository)
        {
            this.ticketRepository = ticketRepository;
            this.shopRepository   = new ShopRepository(db);
            this.userRepository   = new TotsUserRepository(db);
        }

        /// GET /Tickets/
        public ActionResult Index(string sortOrder, int? ShopFilter, bool showAll = false)
        {
            // get tickets using TicketService
            TicketIndexViewModel ticketIndexVM = new TicketService()
                .getSortedTickets(ticketRepository, shopRepository, sortOrder, ShopFilter, showAll);

            ViewBag.ThisUserID = HttpContext.User.Identity.GetUserId();

            return View(ticketIndexVM);
        }

        // GET: /Ticket/Details/5
        public ActionResult Details(int id)
        {
            Ticket ticket = ticketRepository.FindById(id);

            if (ticket == null)
            {
                HttpNotFound();
            }

            TicketViewModel ticketVM = Mapper.Map(ticket, new TicketViewModel());

            return View(ticketVM);
        }

        // GET: /Ticket/Create
        public ActionResult Create()
        {
            ViewBag.ShopId = new SelectList(shopRepository.GetAll(), "ShopId", "Name");
            ViewBag.UserListing = new SelectList(userRepository.GetAll(), "Id", "UserName");

            return View();
        }

        // POST: /Ticket/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="TicketId,ShopId,Description")] TicketViewModel ticketVM)
        {
            Ticket ticket = Mapper.Map(ticketVM, new Ticket());

            // set the id as the id of the logged in user
            var currentUser = userRepository.FindById(User.Identity.GetUserId());
            ticket.UserId = currentUser.Id;

            if (ModelState.IsValid)
            {
                ticket.DateCreated = DateTime.Now;
                ticketRepository.Add(ticket);
                ticketRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.ShopId = new SelectList(shopRepository.GetAll(), "ShopId", "Name", ticket.ShopId);
            ViewBag.UserListing = new SelectList(userRepository.GetAll(), "Id", "UserName", ticketVM.UserId);

            return View(ticketVM);
        }

        // GET: /Ticket/Edit/5
        public ActionResult Edit(int id)
        {
            Ticket ticket = db.Tickets.Find(id);

            if (ticket == null)
            {
                return HttpNotFound();
            }

            TicketViewModel ticketVM = Mapper.Map(ticket, new TicketViewModel());

            // TODO: move into viewmodel
            ViewBag.ShopId      = new SelectList(shopRepository.GetAll(), "ShopId", "Name", ticketVM.ShopId);
            ViewBag.UserListing = new SelectList(userRepository.GetAll(), "Id", "UserName", ticketVM.UserId);

            return View(ticketVM);
        }

        // POST: /Ticket/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="TicketId,ShopId,UserId,Description,Status")] TicketViewModel ticketVM)
        {
            if (ModelState.IsValid)
            {
                var ticket = ticketRepository.FindById(ticketVM.TicketId);
                if(ticket == null)
                {
                    return HttpNotFound();
                }

                ticket.DateModified = DateTime.Now;
                ticket.ShopId = ticketVM.ShopId;
                ticket.UserId = ticketVM.UserId;
                ticket.Status = ticketVM.Status;
                ticket.Description = ticketVM.Description;

                ticketRepository.Update(ticket);
                ticketRepository.Save();

                return RedirectToAction("Index");
            }

            // TODO: move into viewmodel
            ViewBag.ShopId      = new SelectList(shopRepository.GetAll(), "ShopId", "Name", ticketVM.ShopId);
            ViewBag.UserListing = new SelectList(userRepository.GetAll(), "Id", "UserName", ticketVM.UserId);

            return View(ticketVM);
        }

        // GET: /Ticket/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: /Ticket/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            ticketRepository.Delete(ticket);
            ticketRepository.Save();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Returns the string representation of the status code.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        /// <returns>The string representation of the status code.</returns>
        public static string StatusAsAString(int statusCode)
        {
            string status;

            switch (statusCode)
            {
                case 0:
                    status = "Waiting for approval";
                    break;
                case 1:
                    status = "Approved";
                    break;
                case 2:
                    status = "Disapproved";
                    break;
                case 3:
                    status = "Cancelled";
                    break;
                case 4:
                    status = "Completed";
                    break;
                default:
                    status = "Invalid status code";
                    break;
            }

            return status;
        }

        // implement IDisposable
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
